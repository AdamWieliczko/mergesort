#include <iostream>
using namespace std;

class MergeSort
{
public:

	int array[15] = { 9, 3, 5, 4, 2, 11, 11, 11, 11, 11, 55, 33, 22, 56, 79 };
	int p = 0;
	int r = 14;

	void scalanie(int array[], int p, int q, int r)
	{
		int lengthOne = q - p + 1, lengthTwo = r - q, left[lengthOne], right[lengthTwo];

		for (int i = 0; i < lengthOne; i++)
		{
			left[i] = array[p + i];
		}

		for (int j = 0; j < lengthTwo; j++)
		{
			right[j] = array[q + 1 + j];
		}

		int i = 0, j = 0, k = p;

		while (i < lengthOne && j < lengthTwo)
		{
			if (left[i] <= right[j])
			{
				array[k] = left[i];
				i++;
			}
			else
			{
				array[k] = right[j];
				j++;
			}
			k++;
		}

		while (i < lengthOne)
		{
			array[k] = left[i];
			i++; k++;
		}

		while (j < lengthTwo)
		{
			array[k] = right[j];
			j++; k++;
		}
	}


	void sortowanie_przez_scalanie(int array[], int p, int r)
	{
		if (p < r)
		{
			int q = (p + r) / 2;
			sortowanie_przez_scalanie(array, p, q);
			sortowanie_przez_scalanie(array, q + 1, r);
			scalanie(array, p, q, r);
		}
	}

};


int main()
{
	MergeSort something;
	something.sortowanie_przez_scalanie(something.array, something.p, something.r);

	for (int n = 0; n < 15; n++)
	{
		cout << something.array[n] << " ";
	}
}